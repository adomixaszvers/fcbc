from parser import Parser, ParserError
from fc import ForwardChaining
import sys

if __name__ == "__main__":
    try:
        if len(sys.argv) > 2:
            sys.stdout = open(sys.argv[2], 'w')
        parser = Parser(sys.argv[1])
        a = parser.parse()
        print("Duota: ", ", ".join(a[0]))
        print("Taisyklės: ")
        for taisykle in a[1]:
            print(taisykle)
        print("Tikslas: ", ", ".join(a[2]))
        fc = ForwardChaining(*a)
        fc.solve()
        sys.stdout.close()
    except ParserError as e:
        print(e)
        sys.stdout.close()
