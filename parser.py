from rule import Rule

class ParserError(Exception):
    pass


class Parser(object):
    def __init__(self, filename):
        fileobj = open(filename, "r")
        self.lines = fileobj.readlines()
        fileobj.close()

    def parse(self):
        status = "neutral"
        duota = []
        taisykles = []
        tikslas = []
        taisykliu = 0
        for line in self.lines:
            stripped = line.strip()
            index = stripped.find("//")
            if index != -1:
                data = stripped[:index]
                data = data.strip()
                comment = stripped[index:]
            else:
                data = stripped
                comment = ""
            if data == "Duota:":
                status = "duota"
            elif data == "Taisyklės:":
                status = "taisykles"
            elif data == "Tikslas:":
                status = "tikslas"
            elif status == "duota" and data != "":
                duota.extend(data.split(" "))
            elif status == "taisykles" and data != "":
                items = data.split(" ")
                if len(items) < 2:
                    raise ParserError('Netinkama taisyklė {}'.format(items))
                left = items[1:]
                right = items[0]
                taisykliu = taisykliu + 1
                name = "R{}".format(taisykliu)
                rule = Rule(left, right, comment, name)
                taisykles.append(rule)
            elif status == "tikslas" and data != "":
                tikslas.extend(data.split(" "))
        if len(tikslas) > 1:
            raise ParserError('Per daug tikslų {}'.format(tikslas))
        if len(duota) < 1:
            raise ParserError('Nėra faktų')
        if len(taisykles) < 1:
            raise ParserError('Nėra taisyklių')
        if len(tikslas) < 1:
            raise ParserError('Nėra tikslo')
        return duota, taisykles, tikslas
