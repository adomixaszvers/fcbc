class ForwardChaining(object):
    def __init__(self, duota, taisykles, tikslas):
        self.duota = duota
        self.taisykles = taisykles
        self.tikslas = tikslas

    def solve(self):
        iteracija = 1
        pritaikytos = []
        yra = False
        senas = self.duota[:]
        if set(self.tikslas).issubset(set(self.duota)):
            yra = True
        while len(self.taisykles) != 0 and not yra:
            print()
            print("Iteracija", iteracija)
            iteracija = iteracija + 1
            nr = 1
            for taisykle in self.taisykles:
                if taisykle.flag1:
                    print("\t{}. ".format(nr), end='')
                    print(taisykle, "jau buvo taikyta (flag1).")
                    nr = nr + 1
                    continue
                if taisykle.flag2:
                    print("\t{}. ".format(nr), end='')
                    print(taisykle, "praleidžiame (flag2)")
                    nr = nr + 1
                    continue
                if set(taisykle.left).issubset(set(self.duota)):
                    print("\t{}. ".format(nr), end='')
                    if set(taisykle.right).issubset(set(self.duota)):
                        taisykle.flag2 = True
                        print(taisykle, "netaikoma, nes konsekventas yra faktų"
                              "aibėje, ir pažymima flag2.")
                        nr = nr + 1
                        continue
                    else:
                        taisykle.flag1 = True
                        print(taisykle, " taikoma ir pažymima flag1.")
                        self.duota.append(taisykle.right)
                        pritaikytos.append(taisykle)
                        print("\t\tFaktų aibė:", ', '.join(self.duota))
                        if set(self.tikslas).issubset(set(self.duota)):
                            yra = True
                        break
                else:
                    print("\t{}. ".format(nr), end='')
                    print(taisykle, "netaikoma, nes trūksta",
                          ", ".join(set(taisykle.left) - set(self.duota)))
                nr = nr + 1
            if set(senas) == set(self.duota):
                break
            else:
                senas = self.duota[:]
        print()
        print("Atsakymas: ", end='')
        if set(self.tikslas).issubset(set(self.duota)):
            if len(pritaikytos) == 0:
                print("Tikslas pasiektas iš karto")
            else:
                print("Tiklas pasiektas")
                print("Planas: ", end='')
                print(', '.join([taisykle.name for taisykle in pritaikytos]))
        else:
            print("Tikslas nepasiektas")
