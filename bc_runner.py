from parser import Parser, ParserError
from bc import BackwardsChaining
import sys

if __name__ == "__main__":
    try:
        if len(sys.argv) > 2:
            sys.stdout = open(sys.argv[2], 'w')
        parser = Parser(sys.argv[1])
        a = parser.parse()
        print("Duota: ", ", ".join(a[0]))
        print("Taisyklės: ")
        for taisykle in a[1]:
            print(taisykle)
        print("Tikslas: ", ", ".join(a[2]))
        print()
        bc = BackwardsChaining(*a)
        if bc.solve(bc.tikslas[0], 0):
            print()
            print('Atsakymas: tikslas {} pasiektas.'.format(bc.tikslas[0]))
            print('Planas: ', ', '.join([_.name for _ in bc.sprendimas]))
        else:
            print()
            print('Atsakymas: tikslas {} nepasiektas.'.format(bc.tikslas[0]))
        sys.stdout.close()
    except ParserError as e:
        print(e)
        sys.stdout.close()
