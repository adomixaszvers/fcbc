class Rule(object):
    def __init__(self, left, right, comment, name):
        self.left = left
        self.right = right
        self.comment = comment.strip('/')
        self.flag1 = False
        self.flag2 = False
        self.name = name

    def __str__(self):
        return "{}: {} -> {}".format(self.name, ", ".join(self.left), self.right)

    def __repr__(self):
        return self.__str__()
