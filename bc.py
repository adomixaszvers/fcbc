class BackwardsChaining(object):
    def __init__(self, duota, taisykles, tikslas):
        self.duota = duota
        self.taisykles = taisykles
        self.tikslas = tikslas
        self.laikini_tikslai = []
        self.laikini_duoti = []
        self.sprendimas = []
        self.counter = 0

    def numeris(self):
        self.counter = self.counter + 1
        return "{}. ".format(self.counter)

    def solve(self, tikslas, gylis):
        tarpai = ' ' * gylis * 2
        if set(tikslas).issubset(set(self.duota)):
            print(self.numeris(), tarpai, 'Tikslas ', tikslas,
                  '. Faktas (duota).', sep='')
            return True
        if set(tikslas).issubset(set(self.laikini_duoti)):
            print(self.numeris(), tarpai, 'Tikslas ', tikslas,
                  '. Faktas (buvo gautas).', sep='')
            return True
        if set(tikslas).issubset(self.laikini_tikslai):
            print(self.numeris(), tarpai, 'Tikslas ', tikslas,
                  '. Ciklas.', sep='')
            return False
        # print(' ' * gylis * 2, tikslas, 'bandome isvesti')
        self.laikini_tikslai.append(tikslas)
        # print(' ' * gylis * 2, self.laikini_tikslai)
        sprendimas = self.sprendimas[:]
        for taisykle in self.taisykles:
            if taisykle not in self.sprendimas:
                # print(taisykle.right, tikslas)
                if taisykle.right == tikslas:
                    print(self.numeris(), tarpai, 'Tikslas ', tikslas,
                          '. Randame ', taisykle, '. Nauji tikslai ',
                          ', '.join(taisykle.left), '.', sep='')
                    pavyko = True
                    salinami = []
                    for faktas in taisykle.left:
                        if not self.solve(faktas, gylis+1):
                            pavyko = False
                            # print(salinami)
                            break
                        salinami.append(faktas)
                    if pavyko:
                        self.laikini_duoti.append(taisykle.right)
                        self.sprendimas.append(taisykle)
                        faktai = self.duota[:]
                        for laikini in self.laikini_duoti:
                            faktai.append(laikini)
                        print(self.numeris(), tarpai, 'Tikslas ', tikslas,
                              '. Faktas (dabar gautas). Faktai ',
                              ', '.join(faktai), '.', sep='')
                        return True
                    else:
                        for i in salinami:
                            try:
                                self.laikini_duoti.remove(i)
                                self.laikini_tikslai.remove(i)
                            except:
                                pass
                        self.sprendimas = sprendimas
        print(self.numeris(), tarpai, 'Tikslas ', tikslas,
              '. Nėra daugiau taisyklių šiam tikslui.', sep='')
        self.laikini_tikslai.remove(tikslas)
        return False
